package de.ftbastler.bukkitgameshelper;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/*
 * A helper for the BukkitGames plugin.
 * @author ftbastler
 */
public class BukkitGamesHelper extends JavaPlugin implements CommandExecutor {	
	
	private Boolean foundKitFile = false;
	private File kitFile = new File("plugins/BukkitGames/kit.yml");
	
	@Override
	public void onEnable() {
		getCommand("bghelper").setExecutor(this);
		
		if(kitFile.exists())
			foundKitFile = true;
		else {
			Bukkit.getServer().broadcast(ChatColor.RED + "WARNING", "bg.admin");
			Bukkit.getServer().broadcast(ChatColor.RED + "BukkitGames Helper couldn't find the kit.yml! Please run BukkitGames at least once before using the helper.", "bg.admin");
		}
		
		if(getServer().getPluginManager().getPlugin("BukkitGames") != null && getServer().getPluginManager().getPlugin("BukkitGames").isEnabled()) {
			Bukkit.getServer().broadcast(ChatColor.RED + "WARNING", "bg.admin");
			Bukkit.getServer().broadcast(ChatColor.RED + "BukkitGames Helper only works when BukkitGames is disabled to avoid conflicts. Please remove BukkitGames temporarily.", "bg.admin");
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
				
				@Override
				public void run() {
					for(Player player : Bukkit.getServer().getOnlinePlayers()) {
						if(player.isOp() || player.hasPermission("bg.admin"))
							player.sendMessage(ChatColor.RED + "BukkitGames Helper only works when BukkitGames is disabled to avoid conflicts. Please remove BukkitGames temporarily if you want to create custom kits.");
					}
				}
			}, 20, 20*15);
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					if(foundKitFile)
						new BukkitGamesHelperGUI(kitFile);
					//else
						//new BukkitGamesHelperGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private String getEnchantsToString(Map<Enchantment, Integer> enchants) {
		String string = null;
		
		for(Entry<Enchantment, Integer> entry : enchants.entrySet()) {
			if(string == null)
				string = entry.getKey().getName().toString() + " " + entry.getValue().toString();
			else
				string = string + ", " + entry.getKey().getName().toString() + " " + entry.getValue().toString();
		}
		
		if(string == null)
			return "No enchants.";
		
		return string;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player))
			return false;
		
		Player player = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("bghelper")) {
			if(!player.isOp() && !player.hasPermission("bg.admin")) {
				player.sendMessage(ChatColor.RED + "You don't have permission.");
				return true;
			}
			
			if(args.length == 0) {
				if(!foundKitFile) {
					player.sendMessage(ChatColor.RED + "Oh no! BukkitGames Helper couldn't find the kit.yml! Please run BukkitGames at least once before using the helper.");
					player.sendMessage(ChatColor.BOLD + "Please visit: " + ChatColor.WHITE + "http://www.bit.ly/Mo63v8");
					return true;
				}
					
				player.sendMessage("");
				player.sendMessage(ChatColor.GREEN + "====== BUKKITGAMES HELPER (by ftbastler) ======");
				player.sendMessage(ChatColor.AQUA + "Steps to create a new kit for the BukkitGames v2:");
				player.sendMessage(ChatColor.DARK_AQUA + " 1. Clear your inventory.");
				player.sendMessage(ChatColor.DARK_AQUA + " 2. Now add the items for the kit you want to create to your inventory. (They can also be enchanted, have a custom name etc.)");
				player.sendMessage(ChatColor.DARK_AQUA + " 3. Choose an item as an icon for the kit and put this item on the far upper right corner of your opened inventory.");
				player.sendMessage(ChatColor.DARK_AQUA + " 4. Now do /bghelper kit <kitname> <cost> <abilityIDs>");
				player.sendMessage("");
				player.sendMessage(ChatColor.GREEN + "For a more detailed how-to, visit: " + ChatColor.WHITE + "http://www.bit.ly/Mo63v8");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("kit")) {
				if(args.length != 4) {
					player.sendMessage(ChatColor.RED + "Usage: /bghelper kit <kitname> <cost> <abilityIDs>");
					return true;
				}
				
				player.sendMessage("");
					
				String name = args[1];
				String cost = args[2];
				String abilities = args[3];
				ItemStack icon = player.getInventory().getItem(17);
				
				if(icon == null) {
					player.sendMessage(ChatColor.RED + "You didn't set up an icon for the kit.");
					return true;
				}
				
				player.getInventory().setItem(17, null);
				
				YamlConfiguration kitConfig = YamlConfiguration.loadConfiguration(kitFile);
				if(kitConfig.getConfigurationSection(name) != null)
					kitConfig.set(name, null);
				
				kitConfig.createSection(name);
				ConfigurationSection kitSection = kitConfig.getConfigurationSection(name);
				
				player.sendMessage(ChatColor.AQUA + "Creating new kit: " + ChatColor.DARK_AQUA + name);
				
				kitSection.createSection("ITEMS");
				ConfigurationSection itemSection = kitSection.getConfigurationSection("ITEMS");
				int index = 0;
				for(ItemStack i : player.getInventory().getContents()) {
					index++;
					
					if(i == null || i.getType() == Material.AIR)
						continue;
					itemSection.set("item" + index, i);
					player.sendMessage(ChatColor.AQUA + "Added item: " + i.getType().toString() + " x" + i.getAmount() + " w/" + getEnchantsToString(i.getEnchantments()));
				}
				
				for(ItemStack i : player.getInventory().getArmorContents()) {
					index++;
					if(i == null || i.getType() == Material.AIR)
						continue;
					itemSection.set("item" + index, i);
					player.sendMessage(ChatColor.AQUA + "Added item: " + i.getType().toString() + " x" + i.getAmount() + " w/" + getEnchantsToString(i.getEnchantments()));
				}
				
				kitSection.set("COST", Integer.parseInt(cost));
				
				String[] abilitiesList = abilities.replace(" ", "").split(",");
				Integer[] abilitiesList2 = new Integer[abilitiesList.length];
				for(int i=0; i<abilitiesList.length; i++) {
					abilitiesList2[i] = Integer.parseInt(abilitiesList[i]);
					player.sendMessage(ChatColor.AQUA + "Added ability: " + abilitiesList[i]);
				}
				
				kitSection.set("ABILITIES", abilitiesList2);
				
				kitSection.set("ICON", icon.getType().toString());
				player.sendMessage(ChatColor.AQUA + "Added icon: " + icon.getType().toString());
				
				player.getInventory().setItem(17, icon);
				
				try {
					kitConfig.save(kitFile);
				} catch (IOException e) {
					player.sendMessage(ChatColor.RED + "Couldn't save the kit.yml config.");
					e.printStackTrace();
					return true;
				}
				player.sendMessage(ChatColor.GREEN + "Done. Checksum: " + index);
			}
			
			return true;
		}
		return true;
	}
	
	public String uploadPaste(String content) {
		OutputStreamWriter writer = null;
		BufferedReader reader = null;

		try {
			/* String body = "text=" + URLEncoder.encode(content, "UTF-8")
					+ "&" + "lang=" + URLEncoder.encode("html5", "UTF-8")
					+ "&" + "expire=" + URLEncoder.encode(60*24*7+"", "UTF-8")
					+ "&" + "title=" + URLEncoder.encode("BukkitGames log (" + calcDate(System.currentTimeMillis()) + ")", "UTF-8")
					+ "&" + "private=" + URLEncoder.encode("1", "UTF-8");
			*/
			
			String body = content;		
					
			URL url = new URL("http://paste.md-5.net/documents"); // http://paste.scratchbook.ch/api/create
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

			writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(body);
			writer.flush();

			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line = reader.readLine();
			writer.close();
			reader.close();

		    String re1=".*?";	// Non-greedy match on filler
		    String re2="(?:[a-z][a-z]+)";	// Uninteresting: word
		    String re3=".*?";	// Non-greedy match on filler
		    String re4="((?:[a-z][a-z]+))";	// Word 1

		    Pattern p = Pattern.compile(re1+re2+re3+re4,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		    Matcher m = p.matcher(line);
		    if (m.find())
		    {
		        String tag = m.group(1);
		        return "http://paste.md-5.net/" + tag;
		    } else {
		    	return null;
		    }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
