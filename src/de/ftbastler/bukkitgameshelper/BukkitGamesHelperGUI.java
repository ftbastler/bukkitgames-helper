package de.ftbastler.bukkitgameshelper;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.minecraft.util.org.apache.commons.io.IOUtils;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTree;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.JLabel;

public class BukkitGamesHelperGUI extends JFrame {

	private static final long serialVersionUID = 1275946593122113846L;
	private static JPanel contentPane;
	private static BukkitGamesHelperGUI gui;
	private static File kitFile;
	private static JTextArea txtKitFile;
	private static JTree tree;
	private static DefaultMutableTreeNode treeNode;
	
	public static BukkitGamesHelperGUI getGUI() {
		return gui;
	}

	public BukkitGamesHelperGUI(File f) {
		gui = this;
		kitFile = f;
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception e1) {
				//ignore
			}
		}
		
		setVisible(true);
		setTitle("BukkitGames Helper (GUI)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 981, 728);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setName("BukkitGames Helper");
		setContentPane(contentPane);
		
		renderConfigFile();
	}
	
	public BukkitGamesHelperGUI() {
		new BukkitGamesHelperGUI(openFileDialog());
	}
	
	public static File openFileDialog() {		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Select your kit.yml file...");
		fileChooser.setFileFilter(new FileNameExtensionFilter("YAML files (*.yml, *.yaml)", "yml", "yaml"));
		int returnVal = fileChooser.showOpenDialog(getGUI());
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       return fileChooser.getSelectedFile();
	    } else {
	    	System.exit(0); 
	    	return null;
	    }
	}

	private static int shown = 0;
	private static JLabel lblNoteChangesWill;
	private static void updateConfigView() {
		try {
			updateConfigViewEx();
		} catch (Exception e) {
			treeNode = new DefaultMutableTreeNode("ERROR (Click [here] to refresh)");
			TreeModel m = new DefaultTreeModel(treeNode);
			tree.setModel(m);
			tree.updateUI();
			
			if(shown <= 0) {
				JOptionPane.showMessageDialog(null, "ERROR \n\n" + e.getMessage(), "ERROR: Can not parse YAML file!", JOptionPane.ERROR_MESSAGE);
				
				try {
					
					Highlighter h = txtKitFile.getHighlighter();
	                h.removeAllHighlights();
					
					String txt = e.getMessage();
					String re1=".*?";	// Non-greedy match on filler
				    String re2="(line)";	// Word 1
				    String re3=".*?";	// Non-greedy match on filler
				    String re4="(\\d+)";	// Integer Number 1
		
				    Pattern p = Pattern.compile(re1+re2+re3+re4,Pattern.CASE_INSENSITIVE);
				    Matcher m2 = p.matcher(txt);
				    if (m2.find())
				    {
				        String int1=m2.group(2);
				        txtKitFile.setCaretPosition(txtKitFile.getLineStartOffset(Integer.parseInt(int1)-2));
				    }
				} catch (Exception e1) {
					//ignore
				}
				
				shown = 5;
			}
			shown--;
		}
	}
	
	private static void updateConfigViewEx() throws Exception {
		treeNode.removeAllChildren();
		
		YamlConfiguration kitConfig = new YamlConfiguration();
		
		try {
			kitConfig.loadFromString(txtKitFile.getText());
		} catch (Exception e) {
			throw e;
		}

		treeNode = new DefaultMutableTreeNode("Kits");
				
		try {
			for(String s : kitConfig.getKeys(false)) {
				DefaultMutableTreeNode kit = new DefaultMutableTreeNode(s);
				ConfigurationSection kitSection = kitConfig.getConfigurationSection(s);
				
				DefaultMutableTreeNode items = new DefaultMutableTreeNode("items");
				for(String i : kitSection.getConfigurationSection("ITEMS").getKeys(false)) {
					ItemStack itemStack = kitSection.getConfigurationSection("ITEMS").getItemStack(i);
					items.add(getItemTree(i, itemStack));
				}
				
				DefaultMutableTreeNode abilities = new DefaultMutableTreeNode("abilities");
				for(Integer a : kitSection.getIntegerList("ABILITIES")) {
					DefaultMutableTreeNode ability = new DefaultMutableTreeNode("# " + a);
					
					/*
					try {
						DefaultMutableTreeNode desc = new DefaultMutableTreeNode("description: " + BukkitGamesAPI.getApi().getAbilityDescription(a));
						DefaultMutableTreeNode name = new DefaultMutableTreeNode("name: " + BukkitGamesAPI.getApi().getAbilityName(a));
					
						ability.add(name);
						ability.add(desc);
					} catch(Exception e) {
						//ignore
					}
					*/
					
					abilities.add(ability);
				}
				
				DefaultMutableTreeNode cost = new DefaultMutableTreeNode("cost: " + kitSection.getInt("COST"));
				
				DefaultMutableTreeNode icon = null;
				if(Material.getMaterial(kitSection.getString("ICON")) != null)
					icon = new DefaultMutableTreeNode("icon: " + kitSection.getString("ICON"));
				else
					throw new IllegalArgumentException("The material \"" + kitSection.getString("ICON") + "\" is unknown.");
				
				kit.add(icon);
				kit.add(cost);
				kit.add(abilities);
				kit.add(items);
				treeNode.add(kit);
			}
		} catch(Exception e) {
			throw e;
		}
		
		TreeModel m = new DefaultTreeModel(treeNode);
		tree.setModel(m);
		tree.updateUI();
		
		shown = 0;
	}
	
	private static void renderConfigFile() {		
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{382, 576, 0};
		gbl_contentPane.rowHeights = new int[]{668, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		treeNode = new DefaultMutableTreeNode("Kits");
		tree = new JTree(treeNode);
		tree.setCellRenderer(new OwnRenderer());
		ToolTipManager.sharedInstance().registerComponent(tree);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(true);
		MouseListener ml = new MouseAdapter() {
		    public void mousePressed(MouseEvent e) {
		    	
		        int selRow = tree.getRowForLocation(e.getX(), e.getY());
		        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
		        
		        if(selRow != -1) {
			        
			    	if(selPath.getLastPathComponent().toString().contains("ERROR")) {
			    		shown = 0;
			    		updateConfigView();
			    	}
		        	
		        	if(e.getClickCount() == 2) {
		                if(selPath.getLastPathComponent().toString().contains("material: ") || selPath.getLastPathComponent().toString().contains("icon: ")) {
		                	String name = Material.getMaterial(selPath.getLastPathComponent().toString().replace("material: ", "").replace("icon: ", "")).name();
		                	
		                	try {
								openWebpage(new URL("http://minecraft.gamepedia.com/index.php?search=" + name).toURI());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
		                }
		               
		            } else {
		                String search = selPath.getLastPathComponent().toString() + ":";
		                if(selPath.getLastPathComponent().toString().contains(":"))
		                	search = selPath.getLastPathComponent().toString().split(":")[0] + ":";
		                Highlighter h = txtKitFile.getHighlighter();
		                h.removeAllHighlights();
		                String text = txtKitFile.getText();

		                Boolean found = false;
		                int index = text.indexOf(search);
		                
		                while ( index >= 0 ) {
		                    int len = search.length();
		                    
		                    if(!isCommentLine(index)) {
			                    try {
			                    	if(!found)
			                    		txtKitFile.setCaretPosition(index);
			                    	
									h.addHighlight(index, index+len, DefaultHighlighter.DefaultPainter);
									found = true;
								} catch (BadLocationException e1) {
									e1.printStackTrace();
								}
		                    }
		                    index = text.indexOf(search, index+len);
		                }
		                
		                if(!found) {
		                	search = search.toUpperCase();
		                	index = text.indexOf(search);
		                	
		                	while ( index >= 0 ) {
			                    int len = search.length();
			                    
			                    if(!isCommentLine(index)) {
				                    try {
				                    	if(!found)
				                    		txtKitFile.setCaretPosition(index);
				                    	
										h.addHighlight(index, index+len, DefaultHighlighter.DefaultPainter);
										found = true;
									} catch (BadLocationException e1) {
										e1.printStackTrace();
									}
			                    }
			                    index = text.indexOf(search, index+len);
			                }
		                }
		            }
		        }
		    }
		};
		tree.addMouseListener(ml);
		
		JScrollPane scrollPane = new JScrollPane(tree);
		scrollPane.setVisible(true);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		txtKitFile = new JTextArea();
		txtKitFile.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent arg0) {
				updateConfigView();
			}

			@Override
			public void inputMethodTextChanged(InputMethodEvent event) {
				updateConfigView();
			}
		});
		txtKitFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				shown = 1;
				updateConfigView();
			}
		});
		try {
			txtKitFile.setText(IOUtils.toString(new FileInputStream(kitFile)));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		txtKitFile.setColumns(50);
		
		JScrollPane scrollPane2 = new JScrollPane(txtKitFile);
		scrollPane2.setVisible(true);
		
		GridBagConstraints gbc_scrollPane2 = new GridBagConstraints();
		gbc_scrollPane2.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane2.gridx = 1;
		gbc_scrollPane2.gridy = 0;
		contentPane.add(scrollPane2, gbc_scrollPane2);
		
		lblNoteChangesWill = new JLabel("Note: Changes will not get saved to the kit.yml file automatically.");
		GridBagConstraints gbc_lblNoteChangesWill = new GridBagConstraints();
		gbc_lblNoteChangesWill.gridx = 1;
		gbc_lblNoteChangesWill.gridy = 1;
		contentPane.add(lblNoteChangesWill, gbc_lblNoteChangesWill);
		contentPane.updateUI();
		
		updateConfigView();
	}
	
	private static Boolean isCommentLine(int index) {
		try {
			if(txtKitFile.getText().charAt(txtKitFile.getLineStartOffset(txtKitFile.getLineOfOffset(index))) == '#') {
				return true;
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
	
	private static DefaultMutableTreeNode getItemTree(String name, ItemStack itemStack) {
		DefaultMutableTreeNode item = new DefaultMutableTreeNode(name);
		
		DefaultMutableTreeNode type = new DefaultMutableTreeNode("material: " + itemStack.getType().toString());
		item.add(type);
		
		DefaultMutableTreeNode amount = new DefaultMutableTreeNode("amount: " + itemStack.getAmount());
		item.add(amount);
		
		if(itemStack.hasItemMeta()) {
			DefaultMutableTreeNode itemMeta = new DefaultMutableTreeNode("meta");
			
			for(Entry<String, Object> entry : itemStack.getItemMeta().serialize().entrySet()) {
				DefaultMutableTreeNode s = new DefaultMutableTreeNode(entry.getKey() + ": " + entry.getValue());
				itemMeta.add(s);
			}
			
			item.add(itemMeta);
		}
		
		return item;
	}
}

class OwnRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = -7361003996895552452L;
	private HashMap<String, Icon> icons = new HashMap<String, Icon>();
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);
		
		if(row != -1) {
			setToolTipText("");
			if(value.toString().contains("material: ") || value.toString().contains("icon: ")) {
				setToolTipText("Double-click to open the wiki article about this item.");
			}
	    }
		
		String name = value.toString().replace("icon: ", "").replace("material: ", "");
		if(Material.getMaterial(name) != null) {
			@SuppressWarnings("deprecation")
			String id = Material.getMaterial(name).getId() + "";
			
			downloadIcon(id);
			
        	if(icons.containsKey(id) && icons.get(id) != null)
        		setIcon(icons.get(id));
		} else {
			if(value.toString().contains("cost")) {
				downloadIcon("cost");
				
				if(icons.containsKey("cost") && icons.get("cost") != null)
	        		setIcon(icons.get("cost"));
			}
			if(value.toString().contains("itemMeta")) {
				downloadIcon("92");
				
				if(icons.containsKey("92") && icons.get("92") != null)
	        		setIcon(icons.get("92"));
			}
		}
	    
		return this;
	}
	
	private void downloadIcon(String id) {
		try {
        	if(!icons.containsKey(id)) {
        		URL url = new URL("http://achievecraft.com/resource/icons/" + id + ".png");
        		BufferedImage img = ImageIO.read(url);
        		if(img != null)
        			icons.put(id, new ImageIcon(getScaledImage(img, 16, 16)));
        		else
        			icons.put(id, null);
        	}	
        } catch (Exception e) {
           e.printStackTrace();
        }
	}
	
	private BufferedImage getScaledImage(BufferedImage srcImg, int w, int h){
	    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
	    Graphics2D g2 = resizedImg.createGraphics();
	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(srcImg, 0, 0, w, h, null);
	    g2.dispose();
	    return resizedImg;
	}
}
